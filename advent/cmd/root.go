package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"../lib/file"

	// Challenges
	"../lib/bagger"
	"../lib/compiler"
	"../lib/expenses"
	"../lib/grouper"
	"../lib/jolter"
	"../lib/masker"
	"../lib/passport"
	"../lib/password"
	"../lib/pather"
	"../lib/reciter"
	"../lib/scheduler"
	"../lib/seater/ferry"
	"../lib/seater/plane"
	"../lib/slope"
	"../lib/ticketer"
)

var rootCmd = &cobra.Command{
	Use:   "advent 1s input.txt",
	Short: "Evaluate input for a given 2020 advent day",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		// arg 1 day num (+s for simple, ex 1s)
		// arg 2 filename
		fmt.Printf("Processing %q for day #%s:\n", args[1], args[0])
		lines, err := file.ReadFileLines(args[1])
		if err != nil {
			log.Fatal("error reading file:", err)
		}
		executeDay(args[0], lines)
	},
}

var funcs = map[string]func([]string) string{
	"1":   expenses.Report,
	"1s":  expenses.ReportSimple,
	"2":   password.Validate,
	"2s":  password.ValidateSimple,
	"3":   slope.MultiPath,
	"3s":  slope.SimplePath,
	"4":   passport.Validate,
	"4s":  passport.ValidateSimple,
	"5":   plane.FirstAvailable,
	"5s":  plane.MaxId,
	"6":   grouper.AllSum,
	"6s":  grouper.AtLeastOneSum,
	"7":   bagger.CountDescendants,
	"7s":  bagger.CountAncestors,
	"8":   compiler.RepairAndRun,
	"8s":  compiler.Run,
	"10":  jolter.PossibleConfigurations,
	"10s": jolter.UseAll,
	"11":  ferry.Simulate,
	"11s": ferry.SimulateSimple,
	"12":  pather.SimulateWaypoint,
	"12s": pather.SimulateShip,
	"13":  scheduler.FindScheduleLine,
	"13s": scheduler.Soonest,
	"14":  masker.InitializeV2,
	"14s": masker.Initialize,
	"15":  func(lines []string) string {
		return reciter.Fast(lines, 30000000)
	},
	"15s":  func(lines []string) string {
		return reciter.Recite(lines, 2020)
	},
	"16": ticketer.DepartureValue,
	"16s": ticketer.ErrorRate,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func executeDay(dayNum string, lines []string) {
	f, ok := funcs[dayNum]
	if !ok {
		log.Fatalf("There isn't a solution for %q registered", dayNum)
	}
	fmt.Println(f(lines))
}
