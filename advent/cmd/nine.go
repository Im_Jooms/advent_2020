package cmd

import (
	"fmt"
	"log"
	"strconv"

	"github.com/spf13/cobra"

	"../lib/encrypter"
	"../lib/file"
)

var main9Cmd = &cobra.Command{
	Use:   "9 25 input.txt",
	Short: "Evaluate input for day 9 part 2 of the 2020 advent of code",
	Long: `"Specify the size of the preamble (5 for samples,
	25 for real, and the input file you're passing in.`,
	Args: cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		processFunc(args, "9")
	},
}

var simple9Cmd = &cobra.Command{
	Use:   "9s 25 input.txt",
	Short: "Evaluate input for day 9 part 1 of the 2020 advent of code",
	Long: `"Specify the size of the preamble (5 for samples,
	25 for real, and the input file you're passing in.`,
	Args: cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		processFunc(args, "9s")
	},
}

func init() {
	rootCmd.AddCommand(main9Cmd)
	rootCmd.AddCommand(simple9Cmd)
}

func processFunc(args []string, version string) {
	fmt.Printf("Processing %q for day %s with preamble %s:\n", args[1], version, args[0])
	lines, err := file.ReadFileLines(args[1])
	if err != nil {
		log.Fatal("error reading file:", err)
	}
	pre, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatal("error casting preamble", err)
	}
	out := ""
	switch version {
	case "9":
		out = encrypter.Weakness(lines, pre)
	case "9s":
		out = encrypter.FirstFail(lines, pre)
	}
	fmt.Println(out)
}
