package grouper

import "fmt"

func AtLeastOneSum(lines []string) string {
	sum := 0
	questions := map[rune]bool{}
	for _, line := range lines {
		for _, q := range line {
			questions[q] = true
		}
		if line == "" {
			sum += len(questions)
			questions = map[rune]bool{}
		}
	}
	sum += len(questions)
	return fmt.Sprintf("Sum of yes's: %d\n", sum)
}

func AllSum(lines []string) string {
	sum := 0
	questions := map[rune]int{}
	peeps := 0
	for _, line := range lines {
		peeps++
		for _, q := range line {
			if _, ok := questions[q]; !ok {
				questions[q] = 0
			}
			questions[q]++
		}
		if line == "" {
			peeps--
			for _, v := range questions {
				if v == peeps {
					sum++
				}
			}
			questions = map[rune]int{}
			peeps = 0
		}
	}
	for _, v := range questions {
		if v == peeps {
			sum++
		}
	}
	return fmt.Sprintf("Sum of yes's: %d\n", sum)
}
