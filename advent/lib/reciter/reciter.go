package reciter

import (
	"fmt"
	"strconv"
	"strings"
)

// D15 p1
func Recite(lines []string, offset int) string {
	var numbers []int
	numStrs := strings.Split(lines[0], ",")
	for _, numStr := range numStrs {
		num, _ := strconv.Atoi(numStr)
		numbers = append(numbers, num)
	}

	for i := len(numbers); i < offset; i ++ {
		age := searchBack(numbers)
		numbers = append(numbers,age)
	}
	fmt.Println(numbers)
	return fmt.Sprintf("%d number is : %d\n", offset, numbers[len(numbers)-1])
}

func searchBack(nums []int) int {
	cur := nums[len(nums)-1]
	for i := 0; i < len(nums) - 1; i ++ {
		if (cur == nums[len(nums)-2-i]) {
			return i + 1
		}
	}
	return 0
}

func Fast(lines []string, offset int) string {
	var numbers []int
	numMap := map[int]int{}
	numStrs := strings.Split(lines[0], ",")
	for i, numStr := range numStrs {
		num, _ := strconv.Atoi(numStr)
		numbers = append(numbers, num)
		if i < len(numStrs) - 1 {
			numMap[num] = i
		}
	}

	var oldAge = numbers[len(numbers) - 1]
	for i := len(numbers); i < offset; i ++ {
		age := searchBackMap(numMap, numbers[len(numbers)-1], i)
		numbers = append(numbers,age)
		numMap[oldAge] = i-1
		oldAge = age
	}
	return fmt.Sprintf("%d number is : %d\n", offset, numbers[len(numbers)-1])
}

func searchBackMap(numMap map[int]int, num, curI int) int {
	if i, ok := numMap[num]; ok {
		return curI - i - 1
	}
	return 0
}
