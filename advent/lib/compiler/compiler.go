package compiler

import (
	"fmt"
	"strconv"
	"strings"
)

func Run(lines []string) string {
	acc := 0
	visited := map[int]bool{}
	i := 0
	for !visited[i] && i < len(lines) {
		visited[i] = true
		line := lines[i]
		chunks := strings.Split(line, " ")

		switch chunks[0] {
		case "acc":
			val, _ := strconv.Atoi(chunks[1])
			acc += val
		case "jmp":
			val, _ := strconv.Atoi(chunks[1])
			i += val
			continue
		}
		i++
	}
	return fmt.Sprintf("val of acc: %d\n", acc)
}

func RepairAndRun(lines []string) string {
	visited := map[int]bool{}
	visitedOrdered := []int{}
	i := 0
	fixLocation := -1
	for i != len(lines) {
		if visited[i] {
			inspecting := len(visitedOrdered) - 1
			if fixLocation != -1 {
				inspecting = fixLocation - 1
				visited, visitedOrdered = popVisited(visited, visitedOrdered, inspecting-fixLocation)
			}
			fixLocation = inspecting
			line := lines[visitedOrdered[inspecting]]
			chunks := strings.Split(line, " ")

			if chunks[0] == "jmp" {
				i = visitedOrdered[inspecting] + 1
			} else if chunks[0] == "nop" {
				val, _ := strconv.Atoi(chunks[1])
				i = visitedOrdered[inspecting] + val
			} else {
				i = visitedOrdered[inspecting-1]
			}
			visited, visitedOrdered = popVisited(visited, visitedOrdered, 1)
			continue
		}
		visited[i] = true
		visitedOrdered = append(visitedOrdered, i)

		line := lines[i]
		chunks := strings.Split(line, " ")

		if chunks[0] == "jmp" {
			val, _ := strconv.Atoi(chunks[1])
			i += val
			continue
		}
		i++
	}
	badInd := visitedOrdered[fixLocation]
	swapLine := lines[badInd]
	chunks := strings.Split(swapLine, " ")
	switch chunks[0] {
	case "nop":
		chunks[0] = "jmp"
	case "jmp":
		chunks[0] = "nop"
	}
	lines[badInd] = strings.Join(chunks, " ")
	return Run(lines)
}

func popVisited(visited map[int]bool, ordered []int, popCount int) (map[int]bool, []int) {
	for i := 0; i < popCount; i++ {
		curKey := ordered[len(ordered)-1]
		delete(visited, curKey)
		ordered = ordered[:len(ordered)-1]
	}
	return visited, ordered
}
