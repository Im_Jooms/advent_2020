package jolter

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func UseAll(lines []string) string {
	nums := getNumsSorted(lines)
	conversions := map[int]int{
		1: 0,
		2: 0,
		3: 0,
	}
	for i := 0; i < len(nums)-1; i++ {
		conversions[nums[i+1]-nums[i]]++
	}
	return fmt.Sprintf("Product of 1 and 3 jolt jumps: %d\n", conversions[1]*conversions[3])
}

var seenConfig map[string]int64

func PossibleConfigurations(lines []string) string {
	nums := getNumsSorted(lines)
	seenConfig = map[string]int64{}
	configs := possibilities(nums)
	return fmt.Sprintf("%d\n", configs)
}

func possibilities(nums []int) int64 {
	if val, ok := seenConfig[mapKey(nums)]; ok {
		return val
	}
	if len(nums) < 3 {
		return 1
	}
	if nums[1]-nums[0] == 1 {
		if nums[2]-nums[0] == 2 {
			v1 := possibilities(nums[1:])
			seenConfig[mapKey(nums[1:])] = v1
			v2 := possibilities(nums[2:])
			seenConfig[mapKey(nums[2:])] = v2
			val := v1 + v2
			if len(nums)-3 > 0 && nums[3]-nums[0] == 3 {
				v3 := possibilities(nums[3:])
				seenConfig[mapKey(nums[3:])] = v3
				val += v3
			}
			return val
		}
	}
	if nums[1]-nums[0] == 2 {
		if nums[2]-nums[0] == 3 {
			return possibilities(nums[1:]) + possibilities(nums[2:])
		}
	}
	return possibilities(nums[1:])
}

func validConfiguration(nums []int) bool {
	for i := 0; i < len(nums)-1; i++ {
		if nums[i+1]-nums[i] > 3 {
			return false
		}
	}
	return true
}

func getNumsSorted(lines []string) []int {
	nums := []int{0}
	for _, line := range lines {
		num, _ := strconv.Atoi(line)
		nums = append(nums, num)
	}
	sort.Ints(nums)
	nums = append(nums, nums[len(nums)-1]+3)
	return nums
}

func mapKey(nums []int) string {
	return strings.Trim(strings.Replace(fmt.Sprint(nums), " ", ",", -1), "[]")
}
