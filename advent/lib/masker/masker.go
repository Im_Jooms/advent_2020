package masker

import (
	"fmt"
	"strconv"
	"strings"
)

func Initialize(lines []string) string {
	registers := map[string]int64{}
	mask := map[int]rune{}
	for _, line := range lines {
		chunks := strings.Split(line, " = ")
		if chunks[0] == "mask" {
			mask = processMask(chunks[1])
			continue
		}
		value := strings.TrimSpace(chunks[1])
		result := applyMask(value, mask)
		ind := chunks[0][4 : len(chunks[0])-1]
		registers[ind] = result
	}

	sum := int64(0)
	for _, val := range registers {
		sum += val
	}
	return fmt.Sprintf("Sum of register remnants: %d\n", sum)
}

func InitializeV2(lines []string) string {
	registers := map[int64]int64{}
	var mask string
	for _, line := range lines {
		chunks := strings.Split(line, " = ")
		if chunks[0] == "mask" {
			mask = chunks[1]
			continue
		}
		ind := chunks[0][4 : len(chunks[0])-1]
		value := strings.TrimSpace(chunks[1])
		num, _ := strconv.ParseInt(value, 10, 64)
		results := applyIndMask(ind, mask)
		for _, res := range results {
			registers[res] = num
		}
	}

	sum := int64(0)
	for _, val := range registers {
		sum += val
	}
	return fmt.Sprintf("Sum of register remnants: %d\n", sum)
}

func applyIndMask(index string, mask string) []int64 {
	base10, _ := strconv.ParseInt(index, 10, 64)
	base2 := fmt.Sprintf("%036b", base10)
	newStr := ""
	for i, val := range mask {
		if val == '0' {
			newStr += string(base2[i])
			continue
		}
		newStr += string(val)
	}
	adds := []string{""}
	for _, val := range newStr {
		if val == '1' || val == '0' {
			for i := range adds {
				adds[i] += string(val)
			}
			continue
		}
		newAdds := []string{}
		for i := range adds {
			newAdds = append(newAdds, adds[i]+string('0'))
			newAdds = append(newAdds, adds[i]+string('1'))
		}
		adds = newAdds
	}
	nums := []int64{}
	for _, val := range adds {
		num, _ := strconv.ParseInt(val, 2, 64)
		nums = append(nums, num)
	}
	return nums
}

func processMask(mask string) map[int]rune {
	indecies := map[int]rune{}
	for i, c := range mask {
		if c != 'X' {
			indecies[i] = c
		}
	}
	return indecies
}

func applyMask(value string, mask map[int]rune) int64 {
	base10, _ := strconv.ParseInt(value, 10, 64)
	base2 := fmt.Sprintf("%036b", base10)
	for ind, val := range mask {
		base2 = replaceInString(base2, val, ind)
	}
	num, _ := strconv.ParseInt(base2, 2, 64)
	return num
}

func replaceInString(str string, rep rune, i int) string {
	return str[:i] + string(rep) + str[i+1:]
}
