package ticketer

import (
	"fmt"
	"strconv"
	"strings"
)

// D16 p1
func ErrorRate(lines []string) string {
	fields := map[string][]rule{}
	for _, line := range lines {
		if line == "" {
			break
		}
		field, rules := parseRule(line)
		fields[field] = rules
	}

	// your ticket = fields + empty + label
	//myT := lines[len(fields) + 2]

	sum := 0
	for i := len(fields) + 2 + 3; i < len(lines); i ++ {
		invalidSum := parseTicket(fields, lines[i])
		sum += invalidSum
	}

	return fmt.Sprintf("Invalid Sum: %d\n", sum)
}

type rule struct {
	min int
	max int
}

func (r *rule) contains(i int) bool {
	if i >= r.min && i <= r.max {
		return true
	}
	return false
}

func parseRule(ruleLine string) (string,[]rule) {
	rem := strings.Split(ruleLine, ":")
	field := rem[0]
	rem = strings.Split(rem[1], "or")
	fieldRules := []rule{}
	for _, ranges := range rem {
		rNums := strings.Split(ranges, "-")
		min, _ := strconv.Atoi(strings.TrimSpace(rNums[0]))
		max, _ := strconv.Atoi(strings.TrimSpace(rNums[1]))
		r := rule{
			min:min,
			max:max,
		}
		fieldRules = append(fieldRules, r)
	}

	return field, fieldRules
}

func parseTicket(fields map[string][]rule, ticketLine string) int {
	numStrs := strings.Split(ticketLine, ",")
	invalidSum := 0
	for _, numStr := range numStrs {
		curNum, _ := strconv.Atoi(numStr)

		found := false
		for _, rules := range fields {
			found = checkField(rules, curNum)
			if found {
				break
			}
		}
		if !found {
			invalidSum += curNum
		}
	}
	return invalidSum
}

func checkField(rules []rule, val int) bool {
	for _, rule := range rules {
		if rule.contains(val) {
			return true
		}
	}
	return false
}


func DepartureValue(lines []string) string {
	fields := map[string][]rule{}
	for _, line := range lines {
		if line == "" {
			break
		}
		field, rules := parseRule(line)
		fields[field] = rules
	}

	// Your ticket = fields + empty + label.
	myT, _ := validateTicket(fields, lines[len(fields) + 2])

	validTickets := []ticket{myT}
	for i := len(fields) + 2 + 3; i < len(lines); i ++ {
		t, valid := validateTicket(fields, lines[i])
		if !valid {
			continue
		}
		validTickets = append(validTickets, t)
	}

	notMapped := map[int]bool{}
	for i := 0; i < len(validTickets[0].values); i++ {
		notMapped[i] = true
	}
	fieldInd := map[string]int{}

	counter := 0
	for len(notMapped) > 0  && counter < 15 {
		counter ++
		// Map fields of tickets together.
		for num := range notMapped {
			foundCount := 0
			foundField := ""
			for field, rules := range fields {
				if _, ok := fieldInd[field]; ok {
					continue
				}
				if validateField(rules, validTickets, num) {
					foundField = field
					foundCount ++
				}
			}
			if foundCount == 1 {
				fieldInd[foundField] = num
				delete(notMapped, num)
				fmt.Println(num, " is definitely ", foundField)
			} else if foundCount == 0 {
				fmt.Println(num, " is a bad one ")
				delete(notMapped, num)
			}
		}
	}

	fmt.Println(myT)
	prod := myT.values[fieldInd["departure location"]] *
		myT.values[fieldInd["departure station"]] *
		myT.values[fieldInd["departure platform"]] *
		myT.values[fieldInd["departure track"]] *
		myT.values[fieldInd["departure date"]] *
		myT.values[fieldInd["departure time"]]
	fmt.Printf("%d * %d * %d * %d * %d * %d = %d \n",
		myT.values[fieldInd["departure location"]],
		myT.values[fieldInd["departure station"]],
		myT.values[fieldInd["departure platform"]],
		myT.values[fieldInd["departure track"]],
		myT.values[fieldInd["departure date"]],
		myT.values[fieldInd["departure time"]],
		prod)

	// Multiply values from your ticket for all 6 fields with the word "departure."

	return fmt.Sprintf("departure product: %d\n", prod)
}

type ticket struct {
	values []int
}

func validateTicket(fields map[string][]rule, ticketLine string) (ticket, bool) {
	numStrs := strings.Split(ticketLine, ",")
	ticketNums := []int{}
	for _, numStr := range numStrs {
		curNum, _ := strconv.Atoi(numStr)

		found := false
		for _, rules := range fields {
			found = checkField(rules, curNum)
			if found {
				break
			}
		}
		if !found {
			return ticket{}, false
		}
		ticketNums = append(ticketNums, curNum)
	}
	return ticket{
		values: ticketNums,
	}, true
}

func validateField(fieldRules []rule, tickets []ticket, valInd int) bool {
	for _, t := range tickets {
		found := false
		for _, rule := range fieldRules {
			found = rule.contains(t.values[valInd])
			if found {
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

