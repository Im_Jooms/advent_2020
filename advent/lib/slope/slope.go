package slope

import (
	"fmt"
)

func SimplePath(lines []string) string {
	treesHit := GoDownSlope(lines, 1, 3)
	return fmt.Sprintf("Hit %d trees on the way down", treesHit)
}

func MultiPath(lines []string) string {
	paths := [][]int{
		{1, 1},
		{1, 3},
		{1, 5},
		{1, 7},
		{2, 1},
	}
	sum := 0
	product := 1
	for _, path := range paths {
		treesHit := GoDownSlope(lines, path[0], path[1])
		sum += treesHit
		product *= treesHit
	}

	output := fmt.Sprintf("Hit %d total trees while trying paths down.\n", sum)
	output += fmt.Sprintf("The product of those totals is: %d\n", product)
	return output
}

func GoDownSlope(slope []string, vSpeed, hSpeed int) int {
	curX := 0
	trees := 0
	for y := vSpeed; y < len(slope); y += vSpeed {
		curX += hSpeed
		if slope[y][curX%len(slope[y])] != '.' {
			trees++
		}
	}
	return trees
}
