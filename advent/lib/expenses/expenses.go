package expenses

import (
	"fmt"
	"strconv"
)

// D1 p1
func ReportSimple(lines []string) string {
	nums := []int{}
	for _, l := range lines {
		num, _ := strconv.Atoi(l)
		nums = append(nums, num)
	}

	for i, n := range nums {
		for j := i + 1; j < len(nums); j++ {
			if n+nums[j] == 2020 {
				return fmt.Sprintf("%d * %d = %d", n, nums[j], n*nums[j])
			}
		}
	}

	return ""
}

// D1 p2
func Report(lines []string) string {
	nums := []int{}
	for _, l := range lines {
		num, _ := strconv.Atoi(l)
		nums = append(nums, num)
	}

	for i, n := range nums {
		for j := i + 1; j < len(nums); j++ {
			if n+nums[j] > 2020 {
				continue
			}
			for k := j + 1; k < len(nums); k++ {
				if n+nums[j]+nums[k] == 2020 {
					return fmt.Sprintf("%d * %d * %d = %d", n, nums[j], nums[k], n*nums[j]*nums[k])
				}
			}
		}
	}
	return ""
}
