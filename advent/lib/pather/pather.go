package pather

import (
	"fmt"
	"strconv"
)

func SimulateShip(lines []string) string {
	currentDirection := byte('E')
	x, y := simulateShip(lines, currentDirection)
	return fmt.Sprintf("Location: %d, %d\nManhattan Distance: %d\n", x, y, abs(x)+abs(y))
}

func SimulateWaypoint(lines []string) string {
	currentDirection := byte('E')
	x, y := simulateWaypoint(lines, currentDirection, 10, 1)
	fmt.Printf("Currently at %d, %d\n", x, y)
	return fmt.Sprintf("Location: %d, %d\nManhattan Distance: %d\n", x, y, abs(x)+abs(y))
}

func simulateWaypoint(instructions []string, direction byte, wayX, wayY int) (int, int) {
	curDir := direction
	x := 0
	y := 0
	for _, i := range instructions {
		action := i[0]
		num, _ := strconv.Atoi(i[1:])
		switch action {
		case 'N', 'S', 'W', 'E':
			wayX, wayY = move(action, num, wayX, wayY)
		case 'F':
			x += wayX * num
			y += wayY * num
		case 'L':
			curDir, wayX, wayY = turnAbout(curDir, -num, wayX, wayY)
		case 'R':
			curDir, wayX, wayY = turnAbout(curDir, num, wayX, wayY)
		}
	}
	return x, y
}

func simulateShip(instructions []string, direction byte) (int, int) {
	curDir := direction
	x := 0
	y := 0
	for _, i := range instructions {
		action := i[0]
		num, _ := strconv.Atoi(i[1:])
		switch action {
		case 'N', 'S', 'W', 'E':
			x, y = move(action, num, x, y)
		case 'F':
			x, y = move(curDir, num, x, y)
		case 'L':
			curDir = turn(curDir, -num)
		case 'R':
			curDir = turn(curDir, num)
		}
	}
	return x, y
}

func move(dir byte, num, x, y int) (int, int) {
	switch dir {
	case 'N':
		y += num
	case 'S':
		y -= num
	case 'E':
		x += num
	case 'W':
		x -= num
	}
	return x, y
}

func turn(cur byte, deg int) byte {
	deg += dirToDeg(cur)
	if deg < 0 {
		return degToDir(360 + deg)
	}
	return degToDir(deg % 360)
}

func turnAbout(cur byte, deg, wayX, wayY int) (byte, int, int) {
	curDeg := dirToDeg(cur)
	newDeg := deg + curDeg
	if newDeg < 0 {
		newDeg = 360 + newDeg
	}
	newDeg %= 360
	turns := abs(newDeg-curDeg) / 90
	//fmt.Printf("Was %s (%d) turn (%d, %d) %d degs, which is %d turns clockwise?%b\n", string(cur), curDeg, wayX, wayY, deg, turns, newDeg > curDeg)
	for i := 0; i < turns; i++ {
		wayX, wayY = rotateWaypoint(newDeg > curDeg, wayX, wayY)
	}
	return degToDir(newDeg), wayX, wayY
}

func rotateWaypoint(clockwise bool, x, y int) (int, int) {
	var newX int
	var newY int
	if clockwise {
		newX = y
		newY = -x
	} else {
		newX = -y
		newY = x
	}
	return newX, newY
}

func dirToDeg(d byte) int {
	switch d {
	case 'E':
		return 90
	case 'S':
		return 180
	case 'W':
		return 270
	}
	return 0
}

func degToDir(d int) byte {
	switch d {
	case 90:
		return 'E'
	case 180:
		return 'S'
	case 270:
		return 'W'
	}
	return 'N'
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
