package bagger

import (
	"fmt"
	"strconv"
	"strings"
)

type bag struct {
	contains map[string]int
	parents  []string
}

func CountAncestors(lines []string) string {
	bags := map[string]bag{}
	for _, line := range lines {
		if len(line) < 2 {
			continue
		}
		words := strings.Split(line, " ")
		mainBag := words[0] + " " + words[1]
		words = words[4:]
		for len(words) > 3 {
			nextBag := words[1] + " " + words[2]
			addBag(bags, mainBag, nextBag, 0)
			words = words[4:]
		}
	}
	return strconv.Itoa(len(getAncestors(bags, "shiny gold")))
}

func CountDescendants(lines []string) string {
	bags := map[string]bag{}
	for _, line := range lines {
		if len(line) < 2 {
			continue
		}
		words := strings.Split(line, " ")
		mainBag := words[0] + " " + words[1]
		words = words[4:]
		for len(words) > 3 {
			count, _ := strconv.Atoi(words[0])
			nextBag := words[1] + " " + words[2]
			addBag(bags, mainBag, nextBag, count)
			words = words[4:]
		}
	}
	sum := 0
	for _, count := range getDescendants(bags, "shiny gold") {
		sum += count
	}
	return strconv.Itoa(sum)
}

func addBag(bags map[string]bag, key, newB string, count int) {
	var keyBag bag
	var newBag bag
	var ok bool
	if keyBag, ok = bags[key]; !ok {
		keyBag = bag{}
		keyBag.contains = map[string]int{}
	}
	if newBag, ok = bags[newB]; !ok {
		newBag = bag{}
		newBag.contains = map[string]int{}
	}
	keyBag.contains[newB] = count
	bags[key] = keyBag
	newBag.parents = append(newBag.parents, key)
	bags[newB] = newBag
}

func printBags(bags map[string]bag) {
	for name, theBag := range bags {
		fmt.Printf("Name: %q\n", name)
		fmt.Printf("\tcontains: %v\n", theBag.contains)
		fmt.Printf("\tparents: %v\n", theBag.parents)
	}
}

func getAncestors(bags map[string]bag, start string) map[string]bool {
	ancestors := map[string]bool{}
	// assuming no cycles
	for _, name := range bags[start].parents {
		ancestors[name] = true
		gp := getAncestors(bags, name)
		for k := range gp {
			ancestors[k] = true
		}
	}
	return ancestors
}

func getDescendants(bags map[string]bag, start string) map[string]int {
	children := map[string]int{}
	// assuming no cycles
	for name, count := range bags[start].contains {
		if _, ok := children[name]; !ok {
			children[name] = 0
		}
		children[name] += count
		gc := getDescendants(bags, name)
		for k, num := range gc {
			if _, ok := children[k]; !ok {
				children[k] = 0
			}
			children[k] += num * count
		}
	}
	return children
}
