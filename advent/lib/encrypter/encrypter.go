package encrypter

import (
	"fmt"
	"strconv"
)

func FirstFail(lines []string, preamble int) string {
	num, _ := firstFail(lines, preamble)
	return fmt.Sprintf("First invalid: %d", num)
}

func Weakness(lines []string, preamble int) string {
	failNum, failInd := firstFail(lines, preamble)
	lines = lines[:failInd]
	sum := findContiguous(lines, failNum)
	return fmt.Sprintf("Contiguous first and last added: %d", sum)
}

func firstFail(lines []string, preamble int) (int64, int) {
	nums := []int64{}
	for i := 0; i < preamble; i++ {
		num, _ := strconv.ParseInt(lines[i], 10, 64)
		nums = append(nums, num)
	}
	//fmt.Println("Nums is now:", nums)
	for i := preamble; i < len(lines); i++ {
		if lines[i] == "" {
			continue
		}
		num, _ := strconv.ParseInt(lines[i], 10, 64)
		if !findPair(nums, num) {
			return num, i
		}
		nums = append(nums[1:], num)
		//fmt.Println("Nums is now:", nums)
	}
	return -1, 0
}

func findPair(nums []int64, sum int64) bool {
	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i]+nums[j] == sum {
				return true
			}
		}
	}
	return false
}

func findContiguous(lines []string, sum int64) int64 {
	curSum := int64(0)
	numsIncluded := []int64{}
	for i := 0; i < len(lines)-1; i++ {
		num, _ := strconv.ParseInt(lines[i], 10, 64)
		curSum += num
		numsIncluded = append(numsIncluded, num)
		for curSum > sum {
			curSum -= numsIncluded[0]
			numsIncluded = numsIncluded[1:]
		}
		if curSum == sum {
			return min(numsIncluded) + max(numsIncluded)
		}
	}
	return -1
}

func max(nums []int64) int64 {
	m := nums[0]
	for _, num := range nums {
		if num > m {
			m = num
		}
	}

	return m
}

func min(nums []int64) int64 {
	m := nums[0]
	for _, num := range nums {
		if num < m {
			m = num
		}
	}

	return m
}
