package scheduler

import (
	"fmt"
	"strconv"
	"strings"
)

func Soonest(lines []string) string {
	arrival, _ := strconv.Atoi(lines[0])
	s := strings.Split(lines[1], ",")
	min := -1
	minId := 0
	for _, t := range s {
		if t == "x" {
			continue
		}
		id, _ := strconv.Atoi(t)
		if min == -1 || (id-(arrival%id)) < min {
			min = id - (arrival % id)
			minId = id
		}
	}
	return fmt.Sprintf("Bus #%d will arrive in %d mins: %d\n", minId, min, min*minId)
}

func FindScheduleLine(lines []string) string {
	times := []int{}
	s := strings.Split(lines[1], ",")
	for _, t := range s {
		if t == "x" {
			times = append(times, -1)
			continue
		}
		id, _ := strconv.Atoi(t)
		times = append(times, id)
	}
	step := int64(1)
	time := int64(times[0])
	curInd := 0
	for _, b := range times {
		if b == -1 {
			curInd++
			continue
		}
		for (time+int64(curInd))%int64(b) != 0 {
			time += step
		}
		step *= int64(b)
		curInd++
	}
	return fmt.Sprintf("Bus Schedule line found at %d\n", time)
}
