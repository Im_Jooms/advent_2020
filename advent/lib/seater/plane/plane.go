package plane

import "fmt"

func MaxId(lines []string) string {
	max := 0
	for _, line := range lines {
		row, seat := evaluateSeat(line[:7], line[7:])
		id := row*8 + seat
		if id > max {
			max = id
		}
	}
	return fmt.Sprintf("Overall Max: %d\n", max)
}

func FirstAvailable(lines []string) string {
	possibleSeats := map[int]bool{}
	for i := 0; i < 128*8; i++ {
		possibleSeats[i] = true
	}

	max := 0
	min := 128
	for _, line := range lines {
		row, seat := evaluateSeat(line[:7], line[7:])
		id := row*8 + seat
		if id < min {
			min = id
		}
		if id > max {
			max = id
		}
		possibleSeats[id] = false
	}
	mySeat := -1
	for i := min + 1; i < max-1; i++ {
		if possibleSeats[i] {
			mySeat = i
			break
		}
	}

	return fmt.Sprintf("First Available Seat: %d\n", mySeat)
}

func evaluateSeat(row string, seat string) (int, int) {
	rowNum := evaluateString(row, 'B', 127)
	seatNum := evaluateString(seat, 'R', 7)
	return rowNum, seatNum
}

func evaluateString(row string, high rune, max int) int {
	min := 0
	for _, c := range row {
		min, max = getHalf(min, max, c == high)
	}
	return min
}

func getHalf(min, max int, high bool) (int, int) {
	diff := max - min
	half := min + (diff / 2)
	if high {
		return half + 1, max
	}
	return min, half
}
