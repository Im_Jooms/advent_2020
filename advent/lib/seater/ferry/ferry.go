package ferry

import "fmt"

type seatEvaluator func(seats []string, i, j int) int

func SimulateSimple(seats []string) string {
	changes := 1
	tolerance := 4
	for changes > 0 {
		seats, changes = simulate(seats, nextToSeat, tolerance)
	}
	occ := totalOccupied(seats)
	return fmt.Sprintf("Total Occupied: %d\n", occ)
}

var seatViews map[int][]int
var rowWidth int

func Simulate(seats []string) string {
	changes := 1
	tolerance := 5
	rowWidth = len(seats[0])
	constructSeatViews(seats)
	for changes > 0 {
		seats, changes = simulate(seats, seenFromSeat, tolerance)
	}
	occ := totalOccupied(seats)
	return fmt.Sprintf("Total Occupied: %d\n", occ)
}

func simulate(seats []string, eval seatEvaluator, tolerance int) ([]string, int) {
	newSeats := []string{}
	changes := 0
	for i := 0; i < len(seats); i++ {
		newSeats = append(newSeats, "")
		for j := 0; j < len(seats[i]); j++ {
			occ := eval(seats, i, j)
			newVal, changed := newState(seats[i][j], occ, tolerance)
			if changed {
				changes++
			}
			newSeats[i] += string(newVal)
		}
	}
	return newSeats, changes
}

func constructSeatViews(seats []string) {
	seatViews = map[int][]int{}
	for i := 0; i < len(seats); i++ {
		for j := 0; j < len(seats[i]); j++ {
			seatViews[i*rowWidth+j] = getSeatViews(seats, i, j)
		}
	}
}

func nextToSeat(seats []string, i, j int) int {
	occ := 0
	if i > 0 {
		occ += countOcc(seats[i-1][j])
		if j > 0 {
			occ += countOcc(seats[i-1][j-1])
		}
		if j < len(seats[i-1])-1 {
			occ += countOcc(seats[i-1][j+1])
		}
	}
	if j > 0 {
		occ += countOcc(seats[i][j-1])
	}
	if j < len(seats[i])-1 {
		occ += countOcc(seats[i][j+1])
	}
	if i < len(seats)-1 {
		occ += countOcc(seats[i+1][j])
		if j > 0 {
			occ += countOcc(seats[i+1][j-1])
		}
		if j < len(seats[i+1])-1 {
			occ += countOcc(seats[i+1][j+1])
		}
	}
	return occ
}

func seenFromSeat(seats []string, i, j int) int {
	occ := 0
	for _, sId := range seatViews[idFromSeat(i, j)] {
		r, c := seatFromId(sId)
		if seats[r][c] == '#' {
			occ++
		}
	}
	return occ
}

func idFromSeat(r, c int) int {
	return r*rowWidth + c
}

func seatFromId(id int) (int, int) {
	return id / rowWidth, id % rowWidth
}

func getSeatViews(seats []string, i, j int) []int {
	views := []int{}
	// Check up left
	r := i - 1
	c := j - 1
	for r >= 0 && c >= 0 {
		if seats[r][c] != '.' {
			views = append(views, idFromSeat(r, c))
			break
		}
		r--
		c--
	}
	// Check up
	r = i - 1
	for r >= 0 {
		if seats[r][j] != '.' {
			views = append(views, idFromSeat(r, j))
			break
		}
		r--
	}
	// Check up right
	r = i - 1
	c = j + 1
	for r >= 0 && c < len(seats[r]) {
		if seats[r][c] != '.' {
			views = append(views, idFromSeat(r, c))
			break
		}
		r--
		c++

	}
	// Check left
	c = j - 1
	for c >= 0 {
		if seats[i][c] != '.' {
			views = append(views, idFromSeat(i, c))
			break
		}
		c--
	}
	// Check right
	c = j + 1
	for c < len(seats[i]) {
		if seats[i][c] != '.' {
			views = append(views, idFromSeat(i, c))
			break
		}
		c++
	}
	// Check down left
	r = i + 1
	c = j - 1
	for r < len(seats) && c >= 0 {
		if seats[r][c] != '.' {
			views = append(views, idFromSeat(r, c))
			break
		}
		r++
		c--

	}
	// Check down
	r = i + 1
	for r < len(seats) {
		if seats[r][j] != '.' {
			views = append(views, idFromSeat(r, j))
			break
		}
		r++
	}
	// Check down right
	r = i + 1
	c = j + 1
	for r < len(seats) && c < len(seats[r]) {
		if seats[r][c] != '.' {
			views = append(views, idFromSeat(r, c))
			break
		}
		r++
		c++
	}
	return views
}

func newState(s byte, nearOcc int, tolerance int) (byte, bool) {
	if s == 'L' && nearOcc == 0 {
		return '#', true
	}
	if s == '#' && nearOcc >= tolerance {
		return 'L', true
	}
	return s, false
}

func totalOccupied(seats []string) int {
	occ := 0
	for _, row := range seats {
		for _, s := range row {
			if s == '#' {
				occ++
			}
		}
	}
	return occ
}

func countOcc(s byte) int {
	if s == '#' {
		return 1
	}
	return 0
}

func printSeats(seats []string) {
	for _, row := range seats {
		fmt.Println(row)
	}
}
