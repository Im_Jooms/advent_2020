# Advent Of Code 2020

My implementation of each day.

## CLI

I'm also starting to put everything into
a CLI, but each day that was solved is
left as-is. This is the code I used to
generate the numbers I submitted, along
with the output generated. So yes, the
answers are there. Please don't be
malicious with them. This is purely for
educational purposes.

Usage:
```
go run advent/main.go 5 d5/input.txt
```
