package solution

import (
	"fmt"
)

func DoSomething(lines []string) {
	max := 0
	for _, line := range lines {
		row, seat := evaluateSeat(line[:7], line[7:])
		id := row*8 + seat
		if id > max {
			max = id
		}
	}

	fmt.Printf("Overall Max: %d\n", max)
}

func DoSomething2(lines []string) {
	possibleSeats := map[int]bool{}
	for i := 0; i < 128*8; i++ {
		possibleSeats[i] = true
	}

	max := 0
	min := 128
	for _, line := range lines {
		row, seat := evaluateSeat(line[:7], line[7:])
		id := row*8 + seat
		if id < min {
			min = id
		}
		if id > max {
			max = id
		}
		if !possibleSeats[id] {
			fmt.Printf("Already found %d before\n", id)
		}
		possibleSeats[id] = false
	}
	mySeat := -1
	for i := min + 1; i < max-1; i++ {
		if possibleSeats[i] {
			mySeat = i
			break
		}
	}

	fmt.Printf("My Seat: %d\n", mySeat)
}

func getHalf(min, max int, high bool) (int, int) {
	diff := max - min
	half := min + (diff / 2)
	if high {
		return half + 1, max
	}
	return min, half
}

func evaluateSeat(row string, seat string) (int, int) {
	min := 0
	max := 127
	for _, c := range row {
		high := false
		if c == 'B' {
			high = true
		}
		min, max = getHalf(min, max, high)
	}
	rowNum := min
	min = 0
	max = 7
	for _, c := range seat {
		high := false
		if c == 'R' {
			high = true
		}
		min, max = getHalf(min, max, high)
	}
	return rowNum, min
}
