package solution

import (
	"fmt"
	"strconv"
	"strings"
)

func DoSomething(lines []string) {
	valid := 0
	invalid := 0
	fields := []string{}
	for _, line := range lines {
		rem := strings.Split(line, " ")
		for _, entry := range rem {
			split := strings.Split(entry, ":")
			fields = append(fields, split[0])
		}
		if line == "" {
			if hasRequiredFields(fields) == true {
				valid++
			} else {
				invalid++
			}
			fields = []string{}
		}
	}
	fmt.Printf("Valid count: %d\n", valid)
	fmt.Printf("Invalid count: %d\n", invalid)
}

func hasRequiredFields(fields []string) bool {
	requiredFields := map[string]bool{
		"byr": true,
		"iyr": true,
		"eyr": true,
		"hgt": true,
		"hcl": true,
		"ecl": true,
		"pid": true,
		//"cid": true,
	}
	//fmt.Println("With: ", fields)
	for _, f := range fields {
		if _, ok := requiredFields[f]; ok {
			delete(requiredFields, f)
		}
	}
	//fmt.Println("did not find: ", requiredFields)
	if len(requiredFields) == 0 {
		return true
	}
	return false
}

func DoSomething2(lines []string) {
	valid := 0
	invalid := 0
	fields := map[string]string{}
	for _, line := range lines {
		rem := strings.Split(line, " ")
		for _, entry := range rem {
			if len(entry) <= 3 {
				break
			}
			split := strings.Split(entry, ":")
			fields[split[0]] = split[1]
		}
		if line == "" {
			if hasRequiredFields2(fields) == true {
				valid++
			} else {
				invalid++
			}
			fields = map[string]string{}
		}
	}
	fmt.Printf("Valid count: %d\n", valid)
	fmt.Printf("Invalid count: %d\n", invalid)
}

func hasRequiredFields2(fields map[string]string) bool {
	requiredFields := map[string]func(string) bool{
		"byr": validateBYR,
		"iyr": validateIYR,
		"eyr": validateEYR,
		"hgt": validateHGT,
		"hcl": validateHCL,
		"ecl": validateECL,
		"pid": validatePID,
		//"cid": true,
	}
	//fmt.Println("With: ", fields)
	for f, v := range fields {
		if validF, ok := requiredFields[f]; ok {
			//fmt.Println("Need to check: ", f)
			valid := validF(v)
			if valid {
				delete(requiredFields, f)
			}
		}
	}
	//fmt.Println("did not find: ", requiredFields)
	if len(requiredFields) == 0 {
		return true
	}
	return false
}

func validateBYR(txt string) bool {
	year, err := strconv.Atoi(strings.TrimSpace(txt))
	if err != nil || year < 1920 || year > 2002 {
		return false
	}
	return true
}

func validateIYR(txt string) bool {
	year, err := strconv.Atoi(strings.TrimSpace(txt))
	if err != nil || year < 2010 || year > 2020 {
		return false
	}
	return true
}

func validateEYR(txt string) bool {
	year, err := strconv.Atoi(strings.TrimSpace(txt))
	if err != nil || year < 2020 || year > 2030 {
		return false
	}
	return true
}

func validateHGT(txt string) bool {
	if len(txt) < 3 {
		return false
	}
	sub := txt[len(txt)-2:]
	num, err := strconv.Atoi(txt[:len(txt)-2])
	if err != nil {
		return false
	}
	if sub == "cm" && num >= 150 && num <= 193 {
		return true
	}
	if sub == "in" && num >= 59 && num <= 76 {
		return true
	}

	return false
}

func validateHCL(txt string) bool {
	if len(txt) != 7 {
		return false
	}
	if txt[0] != '#' {
		return false
	}
	for i := 1; i < len(txt); i++ {
		if (txt[i] >= '0' && txt[i] <= '9') || (txt[i] >= 'a' && txt[i] <= 'f') {
			continue
		}
		return false
	}
	return true
}

func validateECL(txt string) bool {
	validECLs := map[string]bool{
		"amb": true,
		"blu": true,
		"brn": true,
		"gry": true,
		"grn": true,
		"hzl": true,
		"oth": true,
	}
	if _, ok := validECLs[txt]; !ok {
		return false
	}
	return true
}

func validatePID(txt string) bool {
	if len(txt) != 9 {
		return false
	}
	_, err := strconv.Atoi(txt)
	if err != nil {
		return false
	}
	return true
}
