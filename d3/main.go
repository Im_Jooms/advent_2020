package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"./solution"
)

func main() {
	file, err := os.Open("input.txt")
	//file, err := os.Open("input_sample.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	sc := bufio.NewScanner(file)
	lines := []string{}
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Pt1:")
	solution.DoSomething(lines)
	fmt.Println("\nPt2:")
	solution.DoSomething2(lines)
}
