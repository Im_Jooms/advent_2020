package solution

import (
	"fmt"
)

func DoSomething(lines []string) {
	goDownSlope(lines, 1, 3)
}

func DoSomething2(lines []string) {
	treesHit := goDownSlope(lines, 1, 1)
	treesHit *= goDownSlope(lines, 1, 3)
	treesHit *= goDownSlope(lines, 1, 5)
	treesHit *= goDownSlope(lines, 1, 7)
	treesHit *= goDownSlope(lines, 2, 1)
	fmt.Printf("Invalid Product: %d\n", treesHit)
}

func goDownSlope(slope []string, vSpeed, hSpeed int) int {
	curX := 0
	valid := 0
	invalid := 0
	for y := vSpeed; y < len(slope); y += vSpeed {
		curX += hSpeed
		//fmt.Println("Checking: ", slope[y] , " at pos: ", curX % len(slope[y]), " which is:", slope[y][curX % len(slope[y])])
		if slope[y][curX%len(slope[y])] == '.' {
			valid++
		} else {
			invalid++
		}
	}
	fmt.Printf("Valid count: %d\n", valid)
	fmt.Printf("Invalid count: %d\n", invalid)
	return invalid
}
