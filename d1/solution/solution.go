package solution

import (
	"fmt"
	"strconv"
)

func DoSomething(lines []string) {
	nums := []int{}
	for _, l := range lines {
		num, _ := strconv.Atoi(l)
		nums = append(nums, num)
	}

	for i, n := range nums {
		for j := i + 1; j < len(nums); j++ {
			if n+nums[j] == 2020 {
				fmt.Printf("%d * %d = ", n, nums[j])
				fmt.Println(n * nums[j])
				return
			}
		}
	}
}

func DoSomething2(lines []string) {
	nums := []int{}
	for _, l := range lines {
		num, _ := strconv.Atoi(l)
		nums = append(nums, num)
	}

	for i, n := range nums {
		for j := i + 1; j < len(nums); j++ {
			if n+nums[j] > 2020 {
				continue
			}
			for k := j + 1; k < len(nums); k++ {
				if n+nums[j]+nums[k] == 2020 {
					fmt.Printf("%d * %d * %d = ", n, nums[j], nums[k])
					fmt.Println(n * nums[j] * nums[k])
					return
				}
			}
		}
	}
}
