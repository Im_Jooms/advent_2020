package solution

import (
	"fmt"
	"strconv"
	"strings"
)

func DoSomething(lines []string) {
	valid := 0
	for _, line := range lines {
		rem := strings.Split(line, ":")
		password := strings.TrimSpace(rem[1])
		rem = strings.Split(rem[0], " ")
		passChar := rune(strings.TrimSpace(rem[1])[0])
		rem = strings.Split(rem[0], "-")
		startCount, _ := strconv.Atoi(strings.TrimSpace(rem[0]))
		endCount, _ := strconv.Atoi(strings.TrimSpace(rem[1]))
		count := 0
		for _, c := range password {
			if c == passChar {
				count++
			}
		}
		if count >= startCount && count <= endCount {
			valid++
		}
	}
	fmt.Printf("Valid count: %d\n", valid)
	fmt.Printf("Out of: %d\n", len(lines))
}

func DoSomething2(lines []string) {
	valid := 0
	for _, line := range lines {
		rem := strings.Split(line, ":")
		password := strings.TrimSpace(rem[1])
		rem = strings.Split(rem[0], " ")
		passChar := strings.TrimSpace(rem[1])[0]
		rem = strings.Split(rem[0], "-")
		firstPos, _ := strconv.Atoi(strings.TrimSpace(rem[0]))
		secondPos, _ := strconv.Atoi(strings.TrimSpace(rem[1]))
		firstPos--
		secondPos--
		count := 0
		if firstPos < len(password) && password[firstPos] == passChar {
			count++
		}
		if secondPos < len(password) && password[secondPos] == passChar {
			count++
		}
		if count == 1 {
			valid++
		}
	}
	fmt.Printf("Valid count: %d\n", valid)
	fmt.Printf("Out of: %d\n", len(lines))
}
